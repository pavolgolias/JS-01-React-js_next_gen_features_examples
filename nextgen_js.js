// Some examples of new JS features

// 1. PART - Var -> let / const
console.log("Var -> let / const");

var myName = 'Max';
console.log(myName);
myName = 'Another';
console.log(myName);

let myNameLet = 'Max';
console.log(myNameLet);
myNameLet = 'Another';
console.log(myNameLet);

const myNameConst = 'Max';
console.log(myNameConst);
//myNameConst = 'Another';    // Error
//console.log(myNameConst);


// 2. PART - arrow functions
console.log("arrow functions");

function printMyName(name) {
    console.log(name);
}

const printMyNameAF = (name) => {
    console.log(name);
};

const oneArgExample = name => {
    console.log(name);
};

const noArgExample = () => console.log(name);
const noArgExample2 = () => 2 * 2;

printMyName('Max normal');
printMyNameAF('Max AF');

// 3. PART - exports and imports
console.log("exports and imports");
/*
DEFAULT EXPORTS:
import person from './person.js'
import prs from './person.js'

NAMED EXPORTS:
import {smth} from './utility.js'   // smth is exact name
import {smth as Smth} from './utility.js'
import * as budled from './utility.js'
*/

// 4. PART - classes
console.log("classes");

class Human {
    constructor() {
        this.gender = 'male';
    }

    printGender() {
        console.log(this.gender);
    }
}

class Person extends Human {
    constructor() {
        super();
        this.name = 'Max'
    }

    printMyName() {
        console.log(this.name);
    }
}

const myPerson = new Person();
myPerson.printMyName();
myPerson.printGender();
console.log(myPerson.name);

// new way
/*class Human {
    gender = 'male';

    printGender = () => {
        console.log(this.gender);
    }
}

class Person extends Human {
    name = 'Max';

    printMyName = () => {
        console.log(this.name);
    }
}*/

// 5. PART - Spread and rest operators "..."
console.log("Spread and rest operators \"...\"");
/*
* SPREAD - used to split up array elements or object properties
* REST - user to merge a list of function arguments into an array
* */

// SPREAD usage
const numbers = [1, 2, 3];
let newNumbers = [numbers, 4];
console.log(newNumbers);

newNumbers = [...numbers];
console.log(newNumbers);

newNumbers = [...numbers, 4];
console.log(newNumbers);

const person = {
    name: 'max'
};
const newPerson = {
    ...person,
    age: 28
};
console.log(newPerson);

// REST usage
const filter = (...params) => {
    return params.filter(el => el === 1);
};
console.log(filter(1, 2, 3));

// 6. PART - Desctructuring
console.log("Desctructuring");
[nu1, , nu3] = numbers;
console.log(nu1);
console.log(nu3);

//{name} = {name: 'Max', age: 28};

// 7. PART - References and primitives
console.log("References and primitives");
const number = 1;
const number2 = number;
console.log(number2);   // primitive type, it creates the exact copy, the new number

// objects and arrays are reference types
const prs = {
    name: 'Max'
};

const secPrs = prs;
const secPrs2 = {
    ...prs
};

prs.name = 'changed name';
console.log(secPrs);
console.log(secPrs2);

// 8. PART - array functions
// map, find, findIndex, filter, reduce, concat, slice, splice
console.log("array functions");
const nums = [1, 2, 3];
const doubleNums = nums.map((num) => {
    return num * 2;
});
console.log(nums);
console.log(doubleNums);
